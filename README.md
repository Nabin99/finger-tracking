# Finger Tracking #

* This app tracks the movement of the users fingers on screen. A circle is drawn when user touches on screen, is removed when finger is removes and it follows the finger movement as well.

A custom view class 'MultiTouchView' implements all MotionEvents

A random paint color from predefined colors is used

Static radius of 80 is used to draw circles