package com.apple.fingertracking;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.apple.fingertracking.DataModel.Circle;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by apple on 7/30/17.
 */

public class MultiTouchView extends View {

    private static final float CIRCLE_RADIUS = 80;
    private HashMap<Integer, Circle> circleList;
    private int[] colors = { Color.BLUE, Color.GREEN, Color.MAGENTA,
            Color.BLACK, Color.CYAN, Color.GRAY, Color.RED, Color.DKGRAY,
            Color.LTGRAY, Color.YELLOW };

    public MultiTouchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    private void initialize(){
        circleList = new HashMap<>();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN: {
                createCircle(event);
                break;
            }
            case MotionEvent.ACTION_POINTER_DOWN: {
                createCircle(event);
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                moveCircle(event);
                break;
            }
            case MotionEvent.ACTION_UP: {
                removeCircle(event);
                break;
            }
            case MotionEvent.ACTION_POINTER_UP: {
                removeCircle(event);
                break;
            }
            case MotionEvent.ACTION_CANCEL: {
                removeCircle(event);
                break;
            }
        }
        invalidate();
        return true;
    }

    //creates a circle and adds to circle hashmap
    private void createCircle(MotionEvent event){
        int pointerIndex = event.getActionIndex();
        int circleId = event.getPointerId(pointerIndex);
        Circle circle = new Circle();
        circle.setX(event.getX(pointerIndex));
        circle.setY(event.getY(pointerIndex));
        circle.setRadius(CIRCLE_RADIUS);
        circle.setPaint(getPaint());
        circleList.put(circleId,circle);
    }

    //moves a circle (follows a finger movement)
    private void moveCircle(MotionEvent event){
        int pointerIndex = event.getActionIndex();
        int circleId = event.getPointerId(pointerIndex);
        Circle circle = circleList.get(circleId);
        if (circle != null) {
            circle.setX(event.getX(pointerIndex));
            circle.setY(event.getY(pointerIndex));
        }
    }

    //removes a circle from list
    private void removeCircle(MotionEvent event){
        int pointerIndex = event.getActionIndex();
        int circleId = event.getPointerId(pointerIndex);
        circleList.remove(circleId);
    }

    //returns a paint with random color
    private Paint getPaint(){
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(colors[getRandomIndex()]);
        return  paint;
    }

    //returns a randomIndex of colors array
    private int getRandomIndex(){
        return new Random().nextInt(colors.length);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for(Map.Entry<Integer,Circle> allCircles : circleList.entrySet()){
            Circle circle = allCircles.getValue();
            if(circle != null){
                canvas.drawCircle(circle.getX(),circle.getY(),circle.getRadius(),circle.getPaint());
            }
        }
    }
}
